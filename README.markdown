# Triage Operations

This project is currently being evaluated for use. Contact @ahf for support and
help with getting started with automatic triaging of your Gitlab projects.

## External Documentation

- [The Gitlab Triage Project](https://gitlab.com/gitlab-org/gitlab-triage):
  The official project documentation from Gitlab for the Ruby code that we are
  using for this. This page documents all the properties that are available in
  the YAML files and they also include a short primer on writing Ruby plug-ins
  for the system.

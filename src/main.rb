# Copyright (c) 2021 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

Dir["#{__dir__}/*.rb"].sort.each do |file|
  if file.end_with?("main.rb")
    next
  end

  require_relative File.basename(file, ".rb")
end
